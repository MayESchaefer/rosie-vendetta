# Rosie -  Vendetta theme

Based on [RosieSkies](https://github.com/ArisonID/aris-silly-themes), but with a more text-friendly version of the background image.
